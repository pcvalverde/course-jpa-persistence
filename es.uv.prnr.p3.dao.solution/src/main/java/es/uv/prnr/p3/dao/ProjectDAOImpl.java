package es.uv.prnr.p3.dao;

import java.util.List;

import javax.persistence.EntityManager;

import es.uv.prnr.p2.Employee;
import es.uv.prnr.p2.Project;

public class ProjectDAOImpl extends DAOImpl<Integer, Project> 
 implements ProjectDAO{

	protected ProjectDAOImpl(EntityManager em) {
		super(em, Project.class);
	}

	@Override
	public Project getProjectById(int id) {
		return this.getById(id);
	}

	@Override
	public List<Project> getProjects() {
		return this.findAll();
	}

	@Override
	public void assignTeam(Project p, int startId, int endId) {
		em.getTransaction().begin();
		for(int i=startId; i<=endId; i++) {
			Employee e = em.find(Employee.class,i);
			p.addEmployee(e);
		}
		em.merge(p);
		em.getTransaction().commit();
	}

}
