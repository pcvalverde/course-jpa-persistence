package es.uv.prnr.p3.dao;

public abstract class DAOFactory {
	
	public enum TYPE {JPA,XML}
	
	//Definimos un getDAO para cada entidad
	public abstract EmployeeDAO getEmployeeDAO();
	
	//TODO Crear una clase DAO para la entidad Project
	public abstract ProjectDAO getProjectDAO();

	
	public static DAOFactory getDAOFactory(TYPE t){
		switch(t){
			case JPA:
				return new JPADAOFactory();
			/*case XML:
				return new XMLDAOFactory();*/
		}
		return null;
	}

}
