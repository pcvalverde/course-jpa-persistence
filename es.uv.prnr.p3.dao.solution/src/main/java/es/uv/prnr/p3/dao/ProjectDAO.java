package es.uv.prnr.p3.dao;

import java.util.List;
//TODO Cambiar este import por el que corresponda a vuestro proyecto/clase
import es.uv.prnr.p2.Employee;
import es.uv.prnr.p2.Project;

public interface ProjectDAO {
	   public Project getProjectById(int id);
	   public List<Project> getProjects();
	   public void assignTeam(Project p, int startId, int endId);
}
