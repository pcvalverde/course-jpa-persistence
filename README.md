# A Practical Course on JPA Persistence

A set of coding lectures to learn about SQL, JPA, the DAO pattern and Spring Data. It uses the [Employees MYSQL database](https://github.com/datacharmer/test_db) that must be deployed in the system. The database folder includes a setup with Docker and the extended schemas requested in the course. The connection strings of the projects must be change accordingly. The solutions are implemented using Maven and several Eclipse workspaces.
