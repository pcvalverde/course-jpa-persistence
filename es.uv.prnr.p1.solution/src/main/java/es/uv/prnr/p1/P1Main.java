package es.uv.prnr.p1;

import java.time.LocalDate;

public class P1Main {

	public static void main(String[] args) {
		Employee e = new Employee(1, "Edgar", "Cood", LocalDate.of(1923,8,19), LocalDate.now(), Employee.Gender.M);
		JdbcPersistence db = new JdbcPersistence();
		db.connect();
		db.insertEmployee(e);
		e.setFirstName("Edgar F.");
		db.updateEmployeeFirstName(e);
		db.deleteEmployee(e);
		db.getEmployeesByTitle("Engineer");
		db.getDepartmentManagers("Development");
		return;
	}

}

