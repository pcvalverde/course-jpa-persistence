package es.uv.prnr.p1;
import java.sql.*;

public class JdbcPersistence {
	
	//Creamos la conexi�n como un atributo para ser compartida
	//por el resto de m�todos
	private Connection conn = null;
	private String user = "root";
	private String pass = "DbRelational";
	
	public Connection connect(){
		//TODO Cambiar ip del servidor de la BD y revisar credenciales
		//Formato: driver:tecnologia:ip de la BD: esquema:parametros adicionales
		String connectionURL="jdbc:mysql://192.168.56.101:3306/employees";
		try {
				//El Jar del driver debe ser accesible en el classpath
	            DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
	            //Se crea la conexi�n
	            conn = DriverManager.getConnection(connectionURL,user,pass);
	            //Deshabilitamos que se haga commit por defecto
	            conn.setAutoCommit(false);
	    } catch (SQLException e) {
	            System.out.println(e.getMessage());
	    }
		return conn;
	}
	
	private void runSQLStatement(String sqlQuery){
		Statement stmt;
		try {
			stmt = this.conn.createStatement();
			ResultSet results = stmt.executeQuery(sqlQuery);
		    this.conn.commit();
		    this.printResultSet(results,10);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	private void printResultSet(ResultSet rs, int items) throws SQLException {
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		int rows = 0; 
		while (rs.next() && items > rows) {
			rows ++;
			System.out.print("Row " + rows + ": ");
			for (int i = 1; i <= columnsNumber; i++) {
				if (i > 1) System.out.print(", ");
				String columnValue = rs.getString(i);
				System.out.print(rsmd.getColumnName(i) + " " + columnValue);
			}
			System.out.println("");
		}
	}
	
	public void insertEmployee(Employee e) {
		//Usando ? creamos un lugar para insertar
		String sqlSentence = 
				"INSERT INTO employees (emp_no,birth_date,first_name,last_name,gender,hire_date) "
				+ "VALUES(?,?,?,?,?,?)";
		PreparedStatement pstmt;
		try {
			pstmt = this.conn.prepareStatement(sqlSentence);
			pstmt.setInt(1,e.getId());
			pstmt.setObject(2,e.getBirthDate());
			pstmt.setString(3,e.getFirstName());
			pstmt.setString(4,e.getLastName());
			pstmt.setString(5,e.getGender().name());
			pstmt.setObject(6,e.getHireDate());
			pstmt.executeUpdate();
			this.conn.commit();
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
	}
		
	public void updateEmployeeFirstName(Employee e) {
		String sqlSentence = "UPDATE employees SET first_name = ? WHERE emp_no = ?";
		PreparedStatement pstmt;
		try {
			pstmt = this.conn.prepareStatement(sqlSentence);
			pstmt.setString(1,e.getFirstName());
			pstmt.setInt(2,e.getId());
			pstmt.executeUpdate();
			this.conn.commit();
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
	}
	
	
	public void deleteEmployee(Employee e) {
		String sqlSentence = "DELETE FROM employees WHERE emp_no = ?";
		PreparedStatement pstmt;
		try {
			pstmt = this.conn.prepareStatement(sqlSentence);
			pstmt.setInt(1,e.getId());
			pstmt.executeUpdate();
			this.conn.commit();
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
	}
	
	public void getEmployeesByTitle(String title) {
		String sqlQuery = 
				"Select distinct e.first_name, e.last_name"
				+ " From employees e "
				+ "Join titles t on t.emp_no = e.emp_no "
				+ "Where t.title like '%"+ title +"%'";
		
		this.runSQLStatement(sqlQuery);
	}
	
	public void getDepartmentManagers(String department ) {
		String sqlQuery = 
				"SELECT d.dept_name, dm.from_date, e.first_name, e.last_name"
				+ " FROM dept_manager dm "
				+ "JOIN employees e ON e.emp_no = dm.emp_no "
				+ "JOIN departments d ON d.dept_no =  dm.dept_no "
				+ "WHERE d.dept_name = '" + department + "' "
				+ "ORDER BY dm.from_date ASC";
		this.runSQLStatement(sqlQuery);
	}
	
	public void retrieveEmployees() {
		String sqlQuery = "Select *  from employees";
		Statement stmt;
		ResultSet results;
		try {
			stmt = this.conn.createStatement();
			results = stmt.executeQuery(sqlQuery);
			this.conn.commit();
			//Resultset es un Iterator
			while (results.next()) {
				// Se debe realizar un casting implicito a la hora
				// de recuperar los datos
				System.out.println(results.getString(3));
				System.out.println(results.getString("last_name"));
				System.out.println(results.getDate("birth_date"));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void retrieveEmployee() {
		//Usando ? creamos un lugar para insertar
		String sqlQuery = "Select * from employees" +
				" Where emp_no = ?";
		PreparedStatement pstmt;
		ResultSet result;
		try {
			pstmt = this.conn.prepareStatement(sqlQuery);
			//Incluimos el numero de empleado a buscar
			pstmt.setInt(1,100001);
			result= pstmt.executeQuery();
			this.conn.commit();
			result.next();
			System.out.println(result.getInt("emp_no"));
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void batchInsertDepartment(){
	String departments [] = {"ETSE","DI","ISAW"};
	Integer dept_no=100;
	String insertRow = "INSERT INTO departments (dept_no,dept_name) "
	                + "VALUES(?,?)";
	try {
        PreparedStatement pstmt = conn.prepareStatement(insertRow);
        for (String department: departments) {
        	pstmt.setString(1,'U'+ dept_no.toString());
        	pstmt.setString(2, department);
            pstmt.addBatch();
            dept_no++;
        }
        pstmt.executeBatch();
        this.conn.commit();
        pstmt.close();
    } catch (SQLException ex) {
    	System.out.println(ex.getMessage());
    }
		
	}
	

}
