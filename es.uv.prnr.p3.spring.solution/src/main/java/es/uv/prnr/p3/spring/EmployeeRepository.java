package es.uv.prnr.p3.spring;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import es.uv.prnr.p2.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
	
	// TODO Métodos adicionales para el repositorio empleados
	
	//Búsquedas con condición con distintos operadores
	List<Employee> findByLastNameOrFirstName(String first,String last);
	List<Employee> getByIdLessThan(Integer id);
	List<Employee> readByHireDateBetween (Date d1, Date d2);
	
	//Búsqueda por apellido ordenada por nombre
	List<Employee> findByLastNameOrderByFirstNameAsc(String lastname);
	
	// Uso del operador like
	List<Employee> findByFirstNameNotLike(String likeExpression);
	
	// Búsqueda por apellido sin tener en cuenta mayúsculas
	List<Employee> findByLastNameIgnoreCase(String lastname);
	
	// Uso del operador distinct
	List<Employee> findEmployeeDistinctByFirstName(String firstname);
	
	// Búsqueda a partir de una colección 
	List<Employee> findByFirstNameIn(List<String> names);
	
	// Empleado más jovén (puede usarse tambien First)
	Employee findTopByOrderByBirthDateDesc();
	
	// 10 primeros empleados de cada pagina
	List<Employee> findFirst10ByFirstName(String lastname, Pageable pageable);

	List<Employee> findFirst10ByFirstNameAndLastNameNotLike(char likeExpression1,char likeExpression2,Pageable pageable);
	
}

