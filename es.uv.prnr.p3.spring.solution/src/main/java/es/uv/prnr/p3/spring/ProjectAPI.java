package es.uv.prnr.p3.spring;

import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//TODO cambiar referencias a entidades
import es.uv.prnr.p2.Employee;
import es.uv.prnr.p2.Project;

@Component
public class ProjectAPI {
	
	@Autowired
	private EmployeeRepository employeesRepo;

	//TODO Añadir repositorios adicionales
	private ProjectRepository projectRepository;

	public List<String> projectsByArea (String area){
		List<String> projects = new ArrayList();
		projectRepository.findByAreaIgnoreCase(area).stream().forEach( p -> projects.add(p.getArea()) );
		return projects;
	}

	public List<String> top3BudgetProjects(){
		List<String> top3 = new ArrayList();
		projectRepository.findTop3ByOrderByBudgetDesc().stream().forEach(p -> top3.add(p.getName()));
		return top3;
	}

	public List<String> getProjectTeam (int projectId) {
		Project project = projectRepository.findProjectByIdOrderByTeamDesc(projectId);
		if(project == null || project.getEmployees() == null)
			return null;
		Set<Employee> listEmployees =  project.getEmployees();
		List<String> employeesNames = new ArrayList();
		listEmployees.stream().forEach(e -> employeesNames.add(e.getFirstName()));
		return employeesNames;
	}

	public String employeeInProject (int employeeId, int projectId) {
		String fullName= "Not found";
		Optional<Project>  project  = projectRepository.findById(projectId);
		Optional<Employee> employee=  project.get().getEmployees().stream().filter(e ->e.getId() == employeeId).findFirst();
		fullName = employee.get()!= null ? employee.get().getFirstName(): "Not found";
		return fullName;
	}

	public List<String> employeesLike (char charName, char charSurname, int page){
		List<Employee> employees = employeesRepo.findFirst10ByFirstNameAndLastNameNotLike(charName,charSurname, PageRequest.of(10, page,org.springframework.data.domain.Sort.Direction.ASC,"firstName","lastName"));
		List<String> employeesNames = new ArrayList<>();
		employees.stream().forEach(e -> employeesNames.add(e.getFirstName() +" " + e.getLastName()));
		return employeesNames;

	}

}
