package es.uv.prnr.p3.spring;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import es.uv.prnr.p2.Department;
import es.uv.prnr.p2.Employee;
import es.uv.prnr.p2.Manager;
import es.uv.prnr.p2.Project;
import es.uv.prnr.p2.ProjectService;

@SpringBootApplication
@EntityScan("es.uv.prnr.p2")
public class Application implements CommandLineRunner{
	
	@Autowired
	private EmployeeRepository employeesRepo;
	@Autowired
	private ProjectAPI api;
	
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	@SuppressWarnings("unused")
	@Override
	public void run(String...strings) throws Exception {
		
		List<String> areaProjects = api.projectsByArea("R&D");
		List<String> topProjects = api.top3BudgetProjects();
		
		List<String> projectTeam = api.getProjectTeam(53);
		
	    String employeeFound = api.employeeInProject(10003, 53);
	    
	    List<String> employeeP1Names = api.employeesLike('A','A',0);
	    List<String> employeeP2Names = api.employeesLike('A','A',1);
		
		System.exit(0);
	}
	
	/**
	 * Código auxiliar que genera un conjunto de proyectos a partir del servicio
	 * de la practica 2
	 */
	public void generateProjects() {
		ProjectService service = new ProjectService();
		
		List<Integer> indexes = IntStream.range(0,5)
				.boxed()
                .collect(Collectors.toList());
		
		for( Integer i:indexes) {
			Department proyDepartment = service.getDepartmentById("d00" + (i+1));
			int firstEmployeeId = 10001 + (i*100);
			Manager projectManager = service.promoteToManager(firstEmployeeId, 1000L);
			Project acmeProject = 
					service.createBigDataProject("Persistence Layer " + i,proyDepartment,projectManager,new BigDecimal(1500000.99));
			service.assignTeam(acmeProject,firstEmployeeId,firstEmployeeId + 4);
			service.assignInitialHours(acmeProject.getId());
		}	
	}
	
	/**
	 * Test de utilización del repositorio de empleados
	 */
	public void TestConnection() {
		Employee e = employeesRepo.findById(110022).orElse(null);
		
		List<Employee> q1 = employeesRepo.findByLastNameOrFirstName( "Erde","Georgi");
		List<Employee> q2 = employeesRepo.findFirst10ByFirstName("Georgi", PageRequest.of(10,1));
		List<Employee> q3 = employeesRepo.findEmployeeDistinctByFirstName("Georgi");
		
		Employee kid = employeesRepo.findTopByOrderByBirthDateDesc();
		
		
		//Obtener una lista de empleados de más joven a mas viejo
		List<Employee> youngest = employeesRepo.findAll(
				new Sort(Sort.Direction.DESC, "birthDate","id"));
		
		//Obtener los empleados alfabéticamente por apellido de la posición 20 
		//a la 30
		Page<Employee> topNames = employeesRepo.findAll(
				PageRequest.of(20,30,Sort.Direction.ASC,"lastName"));
		
		
		for(Employee eTop: topNames)
			System.out.println(eTop.getLastName());
		
	}
	


}
