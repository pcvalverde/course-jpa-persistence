package es.uv.prnr.p3.spring;

import java.util.List;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import es.uv.prnr.p2.Project;

public interface ProjectRepository extends JpaRepository<Project, Integer>{

	List<Project> findByAreaIgnoreCase(String area);
	List<Project> findTop3ByOrderByBudgetDesc();
	Project findProjectByIdOrderByTeamDesc(int id);
}
