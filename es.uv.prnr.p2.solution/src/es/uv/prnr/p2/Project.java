package es.uv.prnr.p2;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.*;


@Entity
@Table(name = "project")

@NamedQuery(
			name="Project.findEmployee",
			query = "Select Count(p) > 0  from Project p JOIN p.team t "
					+ "WHERE p.id=?1 AND t.firstName=?2 AND t.lastName=?3"
			)

@NamedQuery(
			name="Project.getTopMonths",
			query="Select h.month, SUM(h.hours) AS emp_hours FROM Project p JOIN p.hours h "
					+ "WHERE p.id=?1 AND h.year=?2 "
					+ "GROUP BY h.month "
					+ "ORDER BY emp_hours DESC "
)

@NamedNativeQuery(
		name="Project.getMonthlyBudget",
		query = "with hour_salary as ("
				+ "SELECT emp_no, salary / 1650 as amount "
				+ "FROM salaries s "
				+ "WHERE to_date = '9999-01-01' "
				+ "), "
				+ "employee_month_cost as ("
				+ "SELECT h.year as year, h.month as month, h.fk_employee, hours * s.amount as employee_cost "
				+ "FROM monthly_hours h "
				+ "INNER JOIN hour_salary s on h.fk_employee = s.emp_no "
				+ "WHERE h.fk_project = ?1 "
				+ ") "
				+ "SELECT year, month, SUM(employee_cost) as budget "
				+ "FROM employee_month_cost "
				+ "GROUP BY year, month ",
		resultSetMapping = "MonthBudgetMapping"
)

@SqlResultSetMapping(
		name="MonthBudgetMapping",
		classes = {
			@ConstructorResult(
				targetClass=MonthlyBudget.class,
				columns= {
						@ColumnResult(name="year"),
						@ColumnResult(name="month"),
						@ColumnResult(name="budget", type=Float.class)
				}
			)	
	}
)

public class Project  {

	@Id @Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "name", unique = true, nullable = false, length = 64)
	private String name;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_department", nullable = false)
	private Department department;
	
	@Column(name = "budget", nullable = false, precision = 10)
	private BigDecimal budget;
	
	@Column(name="start_date")
	private LocalDate startDate;
	
	@Column(name="end_date")
	private LocalDate endDate;
	
	@Column(name = "area", nullable = false, length = 45)
	private String area;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_manager", nullable = false)
	private Manager manager;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "project_team", joinColumns = {
			@JoinColumn(name = "project_id", referencedColumnName="id",
					nullable = false, updatable = false) },
			inverseJoinColumns = {@JoinColumn(name = "employee_id", referencedColumnName="emp_no",
					nullable = false, updatable = false) })
	private Set<Employee> team = new HashSet<Employee>(0);
	
	@OneToMany(fetch = FetchType.LAZY, cascade=CascadeType.PERSIST)
	@JoinColumn(name = "fk_project", updatable=false)
	private List<ProjectHours> hours = new ArrayList<ProjectHours>();
	
	
	public Project() {
	}

	public Project(String name, Department department, Manager manager, BigDecimal budget, LocalDate startDate, LocalDate endDate, String area) {
		this.name = name;
		this.department = department;
		this.manager = manager;
		this.budget = budget;
		this.startDate = startDate;
		this.endDate = endDate;
		this.area = area;
	}
	
	/**
	 * Relaciona el proyecto con el empleado e
	 * @param e
	 */
	public void addEmployee(Employee e) {
		this.team.add(e);
		e.addProject(this);
	}
	
	/**
	 * A�ade un numero de horas al empleado e para un mes-a�o concreto
	 * @param e
	 * @param month
	 * @param year
	 * @param hours
	 */
	public void addHours(Employee e, int month, int year, int hours) {
		if(this.team.contains(e)) {
			ProjectHours assigned = new ProjectHours(month, year, hours, e, this);
			this.hours.add(assigned);
		}
	}

	public int getId() {
		return id;
	}
	
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public Department getDepartment() {
		return this.department;
	}

	public void setDepartment(Department Department) {
		this.department = Department;
	}

	public BigDecimal getBudget() {
		return this.budget;
	}

	public void setBudget(BigDecimal budget) {
		this.budget = budget;
	}

	public String getArea() {
		return this.area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public Manager getManager() {
		return manager;
	}

	public Set<Employee> getEmployees() {
		return this.team;
	}

	
	public List<ProjectHours> getHours(){
		return this.hours;
	}
	
	public void print () {
		System.out.println("Project " + this.name + " from department " + this.department.getDeptName() );
		System.out.print("Managed by ");
		this.manager.print();
		System.out.println("Project Team");
		for(Employee e: this.team) {
			e.print();
		}
	}

}
