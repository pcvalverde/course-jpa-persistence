package es.uv.prnr.p2;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Random;

import javax.persistence.*;


public class ProjectService {
	EntityManagerFactory emf;
	EntityManager em;
	
	public ProjectService() {
		this.emf = Persistence.createEntityManagerFactory("acmeEmployees");
		this.em = emf.createEntityManager();
	}
	
	/**
	 * Busca un departamento
	 * @param id identificador del departamento	
	 * @return entidad con el deparamenteo encontrado
	 */
	public Department getDepartmentById (String id) {
		return em.find(Department.class, id);
	}
	
	/**
	 * Asciende a un empleado a manager. Utilizar una estrateg�a de herencia adecuada
	 * en employee. Tened en cuenta que NO puede haber dos entidades con el mismo id
	 * por lo que habr� que eliminar el empleado original en algun momento.
	 * @param employeeId
	 * @param bonus
	 * @return
	 */
	public Manager promoteToManager(int employeeId, long bonus) {
		em.getTransaction().begin();
		Employee e = em.find(Employee.class, employeeId);
		em.remove(e);
		Manager newManager = new Manager(e,bonus);
		em.persist(newManager);
		em.getTransaction().commit();
		return em.find(Manager.class, newManager.getId());
	}
	
	/**
	 * Crea un nuevo proyecto en el area de Big Data que comienza en la fecha actual y que finaliza
	 * en 3 a�os.
	 * @param name 
	 * @param d departamento asignado al proyecto
	 * @param m manager que asignado al proyecto
	 * @param budget
	 * @return el proyecto creado
	 */
	public Project createBigDataProject(String name, Department d, Manager m, BigDecimal budget) {
		em.getTransaction().begin();
		Project p = new Project(name,d,m,budget,LocalDate.now(),LocalDate.now().plusYears(3),"R&D");
		em.persist(p);
		em.getTransaction().commit();
		return p;
	}
	
	/**
	 * Crea un equipo de proyecto.
	 * @param p proyecto al cual asignar el equipo
	 * @param startId identificador a partir del cual se asignan empleado
	 * @param endId identificador final de empleados. Se asume que start id < endId
	 */
	public void assignTeam (Project p, int startId, int endId) {
		em.getTransaction().begin();
		for(int i=startId; i<=endId; i++) {
			Employee e = em.find(Employee.class,i);
			p.addEmployee(e);
		}
		em.merge(p);
		em.getTransaction().commit();
	}
	
	/**
	 * Calcula el coste m�ximo del proyecto sumando 
	 * @param id
	 * @return
	 */
	public long maxProjectYearlyBudget (int id) {
		Project p = em.find(Project.class, id);
		long budget = 0L;
		for (Employee e: p.getEmployees()){
			budget += e.getCurrentSalary();
		}
		return budget;
	}
	
	/**
	 * Genera un conjunto de horas inicial para cada empleado. El m�todo asigna para cada
	 * mes de duraci�n del proyecto, un n�mero entre 10-165 de horas a cada empleado.
	 * @param projectId
	 * @return total de horas generadas para el proyecto
	 */
	public int assignInitialHours (int projectId) {
		em.getTransaction().begin();
		int totalHours = 0;
		Project p = em.find(Project.class, projectId);
		LocalDate start = p.getStartDate();
		while (start.isBefore(p.getEndDate())) {
			for (Employee e: p.getEmployees()) {
				int hours = new Random().nextInt(165) + 10; 
				totalHours += hours;
				p.addHours(e, start.getMonthValue(),start.getYear(),hours);
			}
			start = start.plusMonths(1);
		}
		em.persist(p);
		em.getTransaction().commit();
		return totalHours;
	}
	
	/**
	 * Busca si un empleado se encuentra asignado en el proyecto
	 * @param projectId
	 * @param firstName
	 * @param lastName
	 * @return cierto si se encuentra asignado al proyecto
	 */
	public boolean employeeInProject (int projectId, String firstName, String lastName){
		Query q = em.createNamedQuery("Project.findEmployee")
			.setParameter(1,projectId)
			.setParameter(2,firstName)
			.setParameter(3,lastName);
		
		return (boolean) q.getSingleResult();
		
	}
	
	/**
	 * Devuelve los meses con mayor n�mero de horas de un a�o determinado
	 * @param projectId
	 * @param year a�o a seleccionar
	 * @param rank n�mero de meses a mostrar, se asume que rank <= 12
	 * @return una lista de objetos mes,hora ordenados de mayor a menor
	 */
	public List getTopHourMonths(int projectId, int year, int rank) {
		Query q = em.createNamedQuery("Project.getTopMonths")
			.setParameter(1,projectId)
			.setParameter(2,year)
			.setMaxResults(rank);
		
		return q.getResultList();
	}
	
	/**
	 * Devuelve para cada par mes-a�o el presupuesto teniendo en cuenta el 
	 * coste/hora de los empleados asociado
	 * @param projectId
	 * @return una colecci�n de objetos MonthlyBudget
	 */
	public List<MonthlyBudget> getMonthlyBudget (int projectId){
		Query q  = em.createNamedQuery("Project.getMonthlyBudget");
		q.setParameter(1, projectId);
		List<MonthlyBudget> result =  q.getResultList();
		return result;
		
	}
	
}
