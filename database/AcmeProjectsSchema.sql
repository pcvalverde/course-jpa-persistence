-- MySQL dump 10.13  Distrib 8.0.14, for Win64 (x86_64)
--
-- Host: 192.168.56.102    Database: acme
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `manager`
--

DROP TABLE IF EXISTS `manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `manager` (
  `emp_no` int(11) NOT NULL,
  `bonus` decimal(10,2) NOT NULL,
  PRIMARY KEY (`emp_no`),
  UNIQUE KEY `emp_no_UNIQUE` (`emp_no`),
  CONSTRAINT `fk_employee_data` FOREIGN KEY (`emp_no`) REFERENCES `employees` (`emp_no`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `monthly_hours`
--

DROP TABLE IF EXISTS `monthly_hours`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `monthly_hours` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `month` int(2) unsigned NOT NULL,
  `year` int(4) unsigned NOT NULL,
  `hours` int(3) unsigned DEFAULT NULL,
  `fk_employee` int(11) NOT NULL,
  `fk_project` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_monthly_hours_employees1_idx` (`fk_employee`),
  KEY `fk_monthly_hours_project1_idx` (`fk_project`),
  CONSTRAINT `fk_monthly_hours_employees1` FOREIGN KEY (`fk_employee`) REFERENCES `employees` (`emp_no`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_monthly_hours_project1` FOREIGN KEY (`fk_project`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5401 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `budget` decimal(10,2) DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `area` varchar(45) NOT NULL,
  `fk_department` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_swedish_ci NOT NULL,
  `fk_manager` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_project_departments1_idx` (`fk_department`) /*!80000 INVISIBLE */,
  KEY `fk_project_manager1_idx` (`fk_manager`) /*!80000 INVISIBLE */,
  CONSTRAINT `fk_project_departments1` FOREIGN KEY (`fk_department`) REFERENCES `departments` (`dept_no`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_project_manager1` FOREIGN KEY (`fk_manager`) REFERENCES `manager` (`emp_no`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_team`
--

DROP TABLE IF EXISTS `project_team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `project_team` (
  `project_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  PRIMARY KEY (`project_id`,`employee_id`),
  UNIQUE KEY `employe_id_UNIQUE` (`employee_id`),
  KEY `fk_project_team_employees1_idx` (`employee_id`),
  CONSTRAINT `fk_project_team_employees1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`emp_no`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_project_team_project1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-16 20:00:38
